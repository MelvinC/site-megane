var stepMinutes = 15;

function bindReservationType() {
	var numberOfCase = 0;

	$('#TypeReservationSelector').on('change', function(e){
		let select = $('#TypeReservationSelector')[0];
		let duration = $(select.options[select.selectedIndex]).attr('time');

		numberOfCase = (duration / stepMinutes) - 1;
	});

	$('.calendarCase').on('mouseover', function(evt){
		if($('#TypeReservationSelector')[0].selectedIndex > 0)
		{
			$('.calendarCase').removeClass('hovered');
			$('.calendarCase').removeClass('impossible');
			let id = evt.target.id;
			let splitId = id.split('-');
			let date = `${splitId[0]}-${splitId[1]}-${splitId[2]}`;
			let classToAdd = 'hovered';
			if($(`#${date}-${parseInt(splitId[3])+numberOfCase}`).length == 0)
			{
				classToAdd = 'impossible';
			}
			else
			{
				for(i=0; i<=numberOfCase; i++)
				{
					let elm = $(`#${date}-${parseInt(splitId[3])+i}`);
					if(elm.hasClass('notPermitted') || elm.hasClass('reserved'))
					{
						classToAdd = 'impossible';
					}
				}
			}

			for(i=0; i<=numberOfCase; i++)
			{
				$(`#${date}-${parseInt(splitId[3])+i}`).addClass(classToAdd);
			}
		}
	});
}

function bindPopup()
{
	$('.popup').draggable();
	$(".calendarCase").on("click", function(evt){
		if($(".hovered").length > 0)
		{
			let startId = evt.target.id;
			let startDate = moment(new Date(startId.substr(0, startId.lastIndexOf('-'))));
			startDate.set("h", 0);
			let startStep = startId.substr(startId.lastIndexOf('-')+1, startId.length);
			startDate.set("m", startStep*stepMinutes);
			let endId = $('.hovered').last().attr('id');
			let endStep = parseInt(endId.substr(endId.lastIndexOf('-')+1, endId.length))+1;
			let endDate = moment(startDate);
			endDate.set("h", 0);
			endDate.set("m", endStep*stepMinutes);
			$("#addAppointmentPopupDetails").html(`Prestation : ${$('#TypeReservationSelector').val()} <br> Date : ${startDate.format('DD/MM/yyyy')} de ${startDate.format('HH:mm')} à ${endDate.format('HH:mm')}.`);
			$("#addAppointmentPopupContainer").show();
		}
	});

	$(".cancelPopupButton").on("click", function(){
		$("#addAppointmentPopupContainer").hide();
	});
}

function initializePage()
{
	bindReservationType();
	bindPopup();
}
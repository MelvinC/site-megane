function doHeader(title) {
	var s = `<div id="top">
				<a id="nom" href="index.html">Mégane Création-Esthétique</a>
				<a id="page" href="index.html">`;
	s = s.concat(title);
	s = s.concat(`</a>
			</div>

			<div id="menu">
				<nav>
				  <ul>
				    <li><a href="Galerie.html">Galerie</a></li>
				    <li class="deroulant"><a>Tarifs &ensp;</a>
				      <ul class="sous">
				        <li><a href="TarifsEsthétique.html">Esthétique</a></li>
				        <li><a href="TarifsOnglerie.html">Onglerie</a></li>
				        <li><a href="TarifsBienEtre.html">Bien-être</a></li>
				      </ul>
				    </li>
				    <li><a href="PrendreRendezVous.html">Prendre rendez-vous</a></li>
				    <li><a href="Contacts.html">Contacts</a></li>
				  </ul>
				</nav>
			</div>`);
	document.getElementById("CommonHeader").innerHTML = s;
}

function DoCustomSelect() {
	var x, i, j, l, ll, selElmnt, a, b, c;
	/*look for any elements with the class "custom-select":*/
	x = document.getElementsByClassName("custom-select");
	l = x.length;
	for (i = 0; i < l; i++) {
	  selElmnt = x[i].getElementsByTagName("select")[0];
	  ll = selElmnt.length;
	  /*for each element, create a new DIV that will act as the selected item:*/
	  a = document.createElement("DIV");
	  a.setAttribute("class", "select-selected");
	  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
	  x[i].appendChild(a);
	  /*for each element, create a new DIV that will contain the option list:*/
	  b = document.createElement("DIV");
	  b.setAttribute("class", "select-items select-hide");
	  for (j = 1; j < ll; j++) {
	    /*for each option in the original select element,
	    create a new DIV that will act as an option item:*/
	    c = document.createElement("DIV");
		c.innerHTML = selElmnt.options[j].innerHTML;
	    if($(selElmnt.options[j]).attr("optionType") == "category")
	    {
	    	$(c).addClass("customOptionCategory");
	    	$(c).append($('<div class="customOptionCategoryButton">-</div>'));
	    }
	    else
	    {
	    	$(c).addClass("customOption");
		    c.addEventListener("click", function(e) {
		        /*when an item is clicked, update the original select box,
		        and the selected item:*/
		        var y, i, k, s, h, sl, yl;
		        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
		        sl = s.length;
		        h = this.parentNode.previousSibling;
		        $(s.options).removeAttr("selected");
		        for (i = 0; i < sl; i++) {
		          if (s.options[i].innerHTML == this.innerHTML) {
		            s.selectedIndex = i;
		            $(s.options[i]).attr("selected", "selected");
		            $(s).trigger("change");
		            h.innerHTML = this.innerHTML;
		            y = this.parentNode.getElementsByClassName("same-as-selected");
		            yl = y.length;
		            for (k = 0; k < yl; k++) {
		              $(y[k]).removeClass("same-as-selected");
		            }
		            $(this).addClass("same-as-selected");
		            break;
		          }
		        }
		        h.click();
		    });
	    }
	    b.appendChild(c);
	  }
	  x[i].appendChild(b);
	  a.addEventListener("click", function(e) {
	      /*when the select box is clicked, close any other select boxes,
	      and open/close the current select box:*/
	      e.stopPropagation();
	      closeAllSelect(this);
	      this.nextSibling.classList.toggle("select-hide");
	      this.classList.toggle("select-arrow-active");
	    });
	}

	function closeAllSelect(elmnt) {
	  /*a function that will close all select boxes in the document,
	  except the current select box:*/
	  var x, y, i, xl, yl, arrNo = [];
	  x = document.getElementsByClassName("select-items");
	  y = document.getElementsByClassName("select-selected");
	  xl = x.length;
	  yl = y.length;
	  for (i = 0; i < yl; i++) {
	    if (elmnt == y[i]) {
	      arrNo.push(i)
	    } else {
	      y[i].classList.remove("select-arrow-active");
	    }
	  }
	  for (i = 0; i < xl; i++) {
	    if (arrNo.indexOf(i)) {
	      x[i].classList.add("select-hide");
	    }
	  }
	}
	/*if the user clicks anywhere outside the select box,
	then close all select boxes:*/
	$(document).on("click", function(evt){
		if(!($(evt.target).hasClass('customOptionCategory') || $(evt.target).parents(".customOptionCategory").length > 0))
		{
			closeAllSelect();
		}
	});

	function showAndHideOption(elmt){
		let button = $(elmt).children('.customOptionCategoryButton');
		if(button.html() == "+")
		{
			button.html("-");
			$(elmt).nextUntil('.customOptionCategory').show();
		}
		else
		{
			button.html("+");
			$(elmt).nextUntil('.customOptionCategory').hide();
		}
	}

	$(".customOptionCategory").on("click", function(evt){
		showAndHideOption(evt.target);
	})
}

function DoPopupConfig()
{
	$(".closePopupButton").on("click", function(evt){
		$(evt.target).parent().parent().parent().hide();
	});
}

function commonInitializePage(title)
{
	doHeader(title);
	DoCustomSelect();
	DoPopupConfig();
}
